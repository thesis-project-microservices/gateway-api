package com.thesis.rotkodr3d;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class GatewayApiMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApiMicroserviceApplication.class, args);
	}

	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder routeLocatorBuilder) {
		return routeLocatorBuilder.routes()
//				.route(p ->p
//					.path("/exreg")
//					.filters(f -> f.addResponseHeader("test", "test"))
//					.uri("http://localhost:8080/my-curriculum-service/exreg"))
				.route(p -> p
					.path("/get")
					.filters(f -> f.addResponseHeader("Hello", "World"))
					.uri("http://httpbin.org:80"))
				.route(p -> p
					.host("*.hystrix.com")
					.filters(f -> f.hystrix(config -> config
							.setName("mycmd")
							.setFallbackUri("forward:/fallback")))
					.uri("http://httpbin.org:80"))
				.build();
	}
	
	@RequestMapping("/fallback")
	public String fallback() {
		return "fallback";
	}
}
