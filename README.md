## gateway-api

This is the gateway-api for the my-faculty microservice architecture.

### Staring the application

To start this app navigate to the project folder and execute ./mvnw spring-boot:run <br>
You can also use the app starter in your IDE, but make sure that it runs as a Spring-Boot app. <br>

### Using the gateway
The application runs on port 8081 <br>
If you want to try out the routes start the my-curriculum service (which runs on port 8080)<br>
And type in your adress-bar the following url `localhost:8081/my-curriculum-service/module`<br>
If everything is wroking correctly should recevie a JSON response with a list which contains all modules
